import React, { Component } from "react";
import Table from "./common/table";
import Like from "./common/like";
import auth from "../services/authService";
import { Link } from "react-router-dom";

class MoviesTable extends Component {
  columns = [
    {
      path: "title",
      lable: "Title",
    },
    { path: "genre.name", lable: "Genre" },
    { path: "numberInStock", lable: "Stock" },
    { path: "dailyRentalRate", lable: "Rate" },
  ];

  editCulumn = {
    key: "Edit",
    content: (movie) => <Link to={`/movies/${movie._id}`}>Edit </Link>,
  };

  likeCulomn = {
    key: "Like",
    content: (movie) => (
      <Like liked={movie.liked} onClick={() => this.props.onLike(movie)} />
    ),
  };

  deleteCulomn = {
    key: "Delete",
    content: (movie) => (
      <button
        onClick={() => this.props.onDelete(movie)}
        className="btn btn-danger  "
      >
        Delete
      </button>
    ),
  };

  constructor() {
    super();
    const user = auth.getCurrentUser();
    if (user) this.columns.push(this.likeCulomn);
    if (user && user.isAdmin) this.columns.push(this.editCulumn);
    if (user && user.isAdmin) this.columns.push(this.deleteCulomn);
  }

  render() {
    const { movies, sortColumn, onSort } = this.props;
    return (
      <Table
        data={movies}
        sortColumn={sortColumn}
        onSort={onSort}
        columns={this.columns}
      />
    );
  }
}

export default MoviesTable;
